package com.example.mvvm.di

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import java.io.IOException
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class NetworkHelper constructor(private val context: Context) {

    fun isInterNetConnected(): Boolean{
        if (isNetworkConnected()){
            try {
                val connection = URL("www.google.com").openConnection() as HttpsURLConnection
                connection.setRequestProperty("User-Agent" , "Test")
                connection.setRequestProperty("Connection" , "close")
                connection.connect()
                return (connection.responseCode == 200)

            }catch (e:IOException){

            }
        }
        return false
    }

    private fun isNetworkConnected() : Boolean{
        var result = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            val networkCapability =  connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(networkCapability) ?: return false

            result = when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true

                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }
        return result
    }
}