package com.example.mvvm.di
import com.example.mvvm.MainActivityViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel {
        MainActivityViewModel(get())
    }


}

