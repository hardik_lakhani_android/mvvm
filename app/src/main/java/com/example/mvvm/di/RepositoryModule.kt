package com.example.mvvm.di
import com.example.mvvm.MainRepository
import com.example.mvvm.api.ApiHelper
import org.koin.dsl.module


val repoModule = module {

    single {
        provideUserRepository(get(), get())
    }


}

fun provideUserRepository(
    apiHelper: ApiHelper,
    networkHelper: NetworkHelper
): MainRepository {
    return MainRepository(apiHelper,  networkHelper)
}



