package com.example.mvvm.di
import android.content.Context

import com.example.mvvm.BuildConfig
import com.example.mvvm.api.ApiHelper
import com.example.mvvm.api.ApiService
import com.example.mvvm.api.ApiServiceImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


val appModule = module {

    single {
        provideNetworkHelper(androidContext())
    }

    single {
        provideOkHttpClient()
    }

    single {

                provideRetrofit(get())

        }



    single {
        provideApiService(get())
    }


    single {
        provideApiHelper(get())
    }


}

private fun provideNetworkHelper(context: Context): NetworkHelper {
    return NetworkHelper(context)
}

private fun provideOkHttpClient(): OkHttpClient {

    var okHttpClient: OkHttpClient? = null

    okHttpClient = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        OkHttpClient.Builder().addInterceptor(loggingInterceptor).readTimeout(
            30,
            TimeUnit.SECONDS
        ).connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30,
            TimeUnit.SECONDS).retryOnConnectionFailure(true).build()
    } else {
        OkHttpClient.Builder().readTimeout(
            30,
            TimeUnit.SECONDS
        ).connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30,
            TimeUnit.SECONDS).retryOnConnectionFailure(true).build()
    }

    return okHttpClient
}

private fun provideRetrofit(
    okHttpClient: OkHttpClient

): Retrofit =
    Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://sd2-hiring.herokuapp.com/api/")
        .client(okHttpClient)
        .build()

private fun provideApiService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}

private fun provideApiHelper(apiService: ApiService): ApiHelper {
    return ApiServiceImpl(apiService)
}




