package com.example.mvvm.di

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}