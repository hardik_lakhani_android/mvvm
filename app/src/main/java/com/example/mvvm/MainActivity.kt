package com.example.mvvm

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvm.databinding.MainActivityBinding
import com.example.mvvm.di.Status
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity<MainActivityBinding,MainActivityViewModel>(R.layout.activity_main),
    LifecycleOwner{

    override val viewModel: MainActivityViewModel by viewModel()

    private lateinit var userListAdapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(viewModel)

        viewModel.fetchUserData(0,10)

        setupObserver()
    }

    private fun setupObserver() {

        viewModel.userList.observe(this, Observer {

            when (it.status) {
                Status.SUCCESS -> {
                    it.data.let { userResponse ->
                        if (userResponse != null) {

                                userListAdapter = userResponse.data?.let { it1 ->
                                    UserListAdapter(
                                        it1.users,this
                                    )
                                }!!
                                rvUserList.layoutManager =
                                    LinearLayoutManager(this, GridLayoutManager.VERTICAL, false)
                                rvUserList.adapter = userListAdapter
                            userListAdapter.notifyDataSetChanged()


                        }
                    }
                }

                Status.LOADING -> {

                }
                Status.ERROR -> {
                    Toast.makeText(this,"Something went wrong!!!",Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


}


