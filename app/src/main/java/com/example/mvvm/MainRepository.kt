package com.example.mvvm

import com.example.mvvm.api.ApiHelper
import com.example.mvvm.api.UserResponseModel
import com.example.mvvm.di.NetworkHelper
import com.example.mvvm.di.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MainRepository(private val apiHelper: ApiHelper, private val networkHelper: NetworkHelper) {

    suspend fun doGetUserApi(
        offset:Int,
        limit:Int
    ): Flow<Resource<UserResponseModel?>> {
        return flow {
            val newSource = apiHelper.getUser(offset, limit)
            if (newSource.isSuccessful) {
                emit(Resource.success(newSource.body()))
            } else {
                emit(Resource.error(newSource.message(), null))
            }
        }
    }

}