package com.example.mvvm

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.databinding.ItemImageListBinding
import kotlinx.android.synthetic.main.item_image_list.view.*

class ImageListAdapter(
    private var imageList: MutableList<String> = mutableListOf(),
    private var mContext: Context?
) : RecyclerView.Adapter<ImageListAdapter.ViewHolder>() {

    var oddCount = 1
    var evenCount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_image_list, parent, false)
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (imageList.size % 2 != 0) {
            if(oddCount < imageList.size-1) {
                mContext?.let {
                    Glide.with(it)
                        .load(imageList[oddCount])
                        .error(R.color.purple_500)
                        .into(holder.itemView.image1)

                }
                oddCount++
                mContext?.let {
                    Glide.with(it)
                        .load(imageList[oddCount])
                        .error(R.color.teal_700)
                        .into(holder.itemView.image2)

                }
                oddCount++
            }
        } else {
            if(evenCount < imageList.size) {
                mContext?.let {
                    Glide.with(it)
                        .load(imageList[evenCount])
                        .error(R.color.purple_500)
                        .into(holder.itemView.image1)

                }
                evenCount++
                mContext?.let {
                    Glide.with(it)
                        .load(imageList[evenCount])
                        .error(R.color.teal_700)
                        .into(holder.itemView.image2)

                }
                evenCount++
            }
        }


    }




    override fun getItemCount(): Int {
        return imageList.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        internal var mListBinding: ItemImageListBinding? = null

    }


}