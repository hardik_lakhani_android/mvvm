package com.example.mvvm

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.mvvm.api.UserResponseModel
import com.example.mvvm.databinding.ItemUserInfoBinding
import kotlinx.android.synthetic.main.item_user_info.view.*

class UserListAdapter(
    private var userList: MutableList<UserResponseModel.Data.Users> = mutableListOf(),
    private var mContext: Context?
) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    private lateinit var imageListAdapter: ImageListAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user_info, parent, false)
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val users = userList[position]


        holder.itemView.tvUserName.text = users.name
        print("image ${users.image}")
        mContext?.let {
            Glide.with(it)
                .load(users.image)
                .error(R.drawable.ic_launcher_background)
                .into(holder.itemView.imgUser)

        }



        if(users.items.size % 2 == 0){
            holder.itemView.ll_odd.visibility = View.GONE
            holder.itemView.rvImageList.visibility = View.VISIBLE

            imageListAdapter = ImageListAdapter(users.items,mContext)
            holder.itemView.rvImageList.layoutManager =
                LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            holder.itemView.rvImageList.adapter = imageListAdapter

        }else{
            holder.itemView.ll_odd.visibility = View.VISIBLE
            holder.itemView.rvImageList.visibility = View.VISIBLE

            mContext?.let {
                Glide.with(it)
                    .load(users.items[0])
                    .error(R.color.black)
                    .apply(
                        RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))

                        .into(holder.itemView.imgUser)

            }
            if(users.items.size == 1){
                holder.itemView.rvImageList.visibility = View.GONE

            }
            imageListAdapter = ImageListAdapter(users.items,mContext)
            holder.itemView.rvImageList.layoutManager =
                LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            holder.itemView.rvImageList.adapter = imageListAdapter
        }
    }




    override fun getItemCount(): Int {
        return userList.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        internal var mListBinding: ItemUserInfoBinding? = null

    }


}