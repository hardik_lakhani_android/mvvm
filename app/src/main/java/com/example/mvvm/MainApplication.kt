package com.example.mvvm

import android.app.Application
import android.content.Context
import com.example.mvvm.di.appModule
import com.example.mvvm.di.repoModule
import com.example.mvvm.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class MainApplication : Application() {
    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()


        startKoin {
            androidContext(this@MainApplication)
            modules(listOf(appModule, repoModule, viewModelModule))
        }

    }

    override fun onLowMemory() {
        super.onLowMemory()

    }
    companion object {
        private var instance: MainApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

}