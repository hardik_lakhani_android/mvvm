package com.example.mvvm

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer

abstract class BaseActivity<BINDING : ViewDataBinding,VIEWMODEL :
Any>(@LayoutRes val layoutId: Int)  : AppCompatActivity() {
    private var isConnected: Boolean = false
    protected lateinit var binding: BINDING
    protected abstract val viewModel: VIEWMODEL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeDataBinding()
        initializeViews()
//        checkNetworkConnectivity()
    }


    open fun initializeViews() {}

    private fun initializeDataBinding() {
        DataBindingUtil.setContentView<BINDING>(this, layoutId)
            .apply {
                binding = this
                lifecycleOwner = this@BaseActivity
                setVariable(1, viewModel)
                executePendingBindings()
            }
    }



}