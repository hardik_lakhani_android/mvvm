package com.example.mvvm.api

import com.google.gson.annotations.SerializedName

data class UserResponseModel(
    @SerializedName("status") var status: Boolean = false,
    @SerializedName("message") var message: String = "",
    @SerializedName("data") var data: Data? = Data()
) {
    data class Data(
        @SerializedName("has_more") var has_more: Boolean = false,
        @SerializedName("users") var users: ArrayList<Users> = ArrayList<Users>(),

        ) {
        data class Users(
            @SerializedName("name") var name: String = "",
            @SerializedName("image") var image: String = "",
            @SerializedName("items") var items: ArrayList<String> = ArrayList<String>()

        )
    }
}