package com.example.mvvm.api

import retrofit2.Response

class ApiServiceImpl(private val apiService: ApiService) : ApiHelper {
    override suspend fun getUser(offset: Int, limit: Int): Response<UserResponseModel> {
        return apiService.getUser(offset, limit)
    }
}