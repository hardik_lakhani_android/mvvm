package com.example.mvvm.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("users?")
    suspend fun getUser(
        @Query("offset") offset:Int,
        @Query("limit") limit:Int
    ): Response<UserResponseModel>
}