package com.example.mvvm.api

import retrofit2.Response

interface ApiHelper {
    suspend fun getUser(
        offset:Int,
        limit:Int
    ):Response<UserResponseModel>
}