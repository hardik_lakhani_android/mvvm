package com.example.mvvm

import androidx.lifecycle.*
import com.example.mvvm.api.UserResponseModel
import com.example.mvvm.di.Resource
import com.example.mvvm.di.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

class MainActivityViewModel(private val mainRepository: MainRepository) : ViewModel(),
    LifecycleObserver {

//    val userList = MutableLiveData<Resource<UserResponseModel>>()
    private val _userListResponse = MutableLiveData<Resource<UserResponseModel>>()

    val userList: LiveData<Resource<UserResponseModel>>
        get() = _userListResponse


    fun fetchUserData(
        offset: Int,
        limit: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            _userListResponse.postValue(Resource.loading(null))
            val user = mainRepository.doGetUserApi(offset, limit)
            user.collect {
                when (it.status) {
                    Status.SUCCESS -> _userListResponse.postValue(Resource.success(it.data))
                    Status.ERROR -> _userListResponse.postValue(
                        Resource.error(
                            it.message ?: "Error Occurred", null
                        )
                    )
                    Status.LOADING -> _userListResponse.postValue(Resource.loading(null))
                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onActivityResume() {

    }

    override fun onCleared() {
        super.onCleared()
    }




}


